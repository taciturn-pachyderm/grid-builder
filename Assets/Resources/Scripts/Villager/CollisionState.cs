using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CollisionState : MonoBehaviour {

    private Targets targets;
    private Work work;
    private Properties properties;
    public bool collided;
    public bool repelled;
    public GameObject collisionObject;
    public float lastCollisionRecheck;
    private float collisionRecheck = 3.0f;
    public float lastRepel = 0.0f;
    private float repelReset = 1.0f;
    private RaycastHit hit;

    public bool move;

    public void Start() {
        properties = GetComponent<Properties>();
        targets = GetComponent<Targets>();
        work = GetComponent<Work>();
        lastCollisionRecheck = Time.time;
    }

    void Update () {
        // if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), 10)) {
        //     print ("hit something");
        // }
        TestRayCastCollision();
        if (Time.time - lastRepel > repelReset) {
            repelled = false;
        }
        if (work.working && work.IsStillWorking()) {
            work.PlayChopSound();
            return;
        }

        // trigger once to start work on collided
        if (collided) {
            Properties targetProps = targets.target.GetComponent<Properties>();
            properties.currentState = DetermineState(targetProps, collisionObject);
            work.Execute(targetProps, collisionObject, properties.currentState);
            ResetCollision();
            return;
        }

        if (work.ProcessWorking()) {
            return;
        }
        if (targets.HasTarget()) {
            // temporarily disabled; it's triggering right after targeting a resource and causing villager to not move toward resource before harvesting
            // targets.CheckForRecollision();

            GetComponent<VillagerController>().Move(targets.target);
            return;
        }
    }

    /*
     * This function determines how to handle the villager on collision.
     * Sequence matters. The first state set is the state that will get executed.
     * Work.Execute() will trigger the appropriate function based on the state.
     * State gets updated each Update cycle if villager has collided with a target.
     */
    public string DetermineState(Properties targetProps, GameObject other) {
        string state = "";
        state = state == "" && !targetProps.targeted && targetProps.type != "storage" ? "ResetTarget" : state;

        // non-storage collision states
        if (other.gameObject.GetInstanceID() == targets.target.GetInstanceID() && targetProps.targeted) {
            state = state == "" && !targetProps.workable ? "CollectTarget" : state;
            state = state == "" && work.haveMaterials && work.building != null ? "AddStock" : state;
            state = state == "" ? "StartWork" : state;
            return state;
        }

        // storage collision states
        if (targetProps.type == "storage" && other.GetComponent<Properties>().type == "storage") {
            state = state == "" && work.haveMaterials && ResourceCounter.counter.resources.Contains(work.material) ? "PutInStorage" : state;
            state = state == "" && work.building != null && !work.haveMaterials ? "GetFromStorage" : state;
        }
        return state;
    }

    public string GetRepr() {
        return Representation.repr.CapitalizeFirstLetter(properties.job) + "/" + Representation.repr.CapitalizeFirstLetter(properties.baseJob) + " (" + properties.id.ToString() + ")" +
            "\n" + (work.working ? "working" : "idle") + " " +
            string.Format("{0:0.0}", (Time.time - work.workStart < 2.0f ? Time.time - work.workStart : 0f)) +
            "\ntarget: " + (targets.target == null ? "" : Representation.repr.CapitalizeFirstLetter(targets.target.name)) +
            "\nbuilding: " + (work.building == null ? "" : Representation.repr.CapitalizeFirstLetter(work.building.name)) +
            "\n" + (work.haveMaterials ? "carrying: " : "finding: ") + work.material +
            "\nstate: " + properties.currentState;
    }

    private void OnCollisionEnter(Collision other) {
        if (other == null) {
            return;
        }
        PreprocessCollision(other.gameObject);
    }

    private void OnCollisionStay(Collision other) {
        PreprocessCollision(other.gameObject);
    }

    private void TestRayCastCollision() {
        float range = 3;
        Debug.DrawRay(transform.position, transform.forward * range, Color.green);
        if (Physics.Raycast(transform.position, transform.forward, out hit, range)) {
            GetComponent<VillagerController>().moveForce = Vector3.back;
            Debug.Log(hit.transform.name);
        }
    }

    private void PreprocessCollision(GameObject other) {
        if ((other.transform.CompareTag("ignorecollision") && other.layer != 9) || repelled) {
            return;
        }
        if (targets.target == null || other.GetInstanceID() != targets.target.GetInstanceID()) {
            collided = false;
            repelled = true;
            lastRepel = Time.time;
            Vector3 newMoveForce = new Vector3(Mathf.Sign(GetComponent<VillagerController>().moveForce.x) * Random.Range(10, 100), 0, Mathf.Sign(GetComponent<VillagerController>().moveForce.z) * Random.Range(10, 100));
            GetComponent<VillagerController>().moveForce = newMoveForce * -1000000;
            return;
        }
        ProcessCollision(other);
    }

    public void ProcessCollision(GameObject other) {
        if (collided || work.working) {
            return;
        }
        if (other == null) {
            return;
        }
        if (targets.target == null || other.GetComponent<Properties>() == null) {
            // nothing to do
            return;
        }
        if (other.gameObject.GetInstanceID() == targets.target.GetInstanceID()) {
            collided = true;
            collisionObject = other;
            GetComponent<VillagerController>().RemoveForce();
        }
    }

    public void CheckForRecollision() {
        /*
            if position has not changed after a set time and target is not null, trigger collision;
            prevents villager from getting stuck on objects when target changes
            while villager is inside collision border
        */
        if (Time.time - lastCollisionRecheck < collisionRecheck) {
            return;
        }
        if (targets.target != null) {
            Vector3 size = new Vector3(GetComponent<CapsuleCollider>().radius, GetComponent<CapsuleCollider>().height, GetComponent<CapsuleCollider>().radius);
            Collider[] check = Physics.OverlapBox(transform.position, size, Quaternion.identity, 1 << 8);
            foreach (Collider c in check) {
                if (targets.target.GetInstanceID() == c.gameObject.GetInstanceID()) {
                    ProcessCollision(targets.target);
                    lastCollisionRecheck = Time.time;
                    return;
                }
            }
        }
    }

    public void ResetCollision() {
        collided = false;
        collisionObject = null;
    }
}
