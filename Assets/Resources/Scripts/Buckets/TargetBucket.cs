using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TargetBucket : MonoBehaviour {
    public static TargetBucket bucket;
    private int resourceCount;
    private int maxResources = 50;
    private HashSet<string> resourcePositions = new HashSet<string>();
    public List<GameObject> targets = new List<GameObject>();
    private float halfWallLength = 20;

    void Awake() {
        // singleton pattern
        if (bucket == null) {
            bucket = this;
        } else if (bucket != this) {
            Destroy(gameObject);
        }
    }

    void Start() {
        SpawnResources();
    }

    void SpawnResources() {
        while (resourceCount < maxResources) {
            Vector3 theVector = GetValidPosition();

            resourceCount++;
            if (TooCloseToOthers(theVector)) {
                continue;
            }

            InstantiateResource(theVector, ResourcePrefabs.resources.GetRandom("raw"));
        }
        print ("made " + targets.Count + " targets");
    }

    Vector3 GetValidPosition() {
        return new Vector3(
            Random.Range(-halfWallLength, halfWallLength),
            0.25f,
            Random.Range(-halfWallLength, halfWallLength)
        );
    }

    bool TooCloseToOthers(Vector3 theVector) {
        string testPositionString = Mathf.RoundToInt(theVector.x * 1.5f) + "," + Mathf.RoundToInt(theVector.z * 1.5f);
        return !resourcePositions.Add(testPositionString);
    }

    public GameObject InstantiateResource(Vector3 theVector, Object theObject) {
        GameObject instance = Instantiate(theObject, theVector, Quaternion.identity) as GameObject;
        instance.name = theObject.name;
        instance.transform.SetParent(transform);
        targets.Add(instance);
        return instance;
    }

    public int CountAll(string target) {
        return targets.Count(n => n != null && n.name == target);
    }
}
