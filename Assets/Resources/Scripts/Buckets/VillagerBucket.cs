using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillagerBucket : MonoBehaviour {
    public static VillagerBucket bucket;
    public Transform villagers;
    private int villagerCount;
    private int maxVillagers = 1;
    public bool spawned = false;

    void Awake() {
        // singleton pattern
        if (bucket == null) {
            bucket = this;
        } else if (bucket != this) {
            Destroy(gameObject);
        }
    }

    void Start() {
        Invoke("SpawnVillagers", 0.1f);
    }

    void SpawnVillagers() {
        villagers = GameObject.Find("GameScripts/VillagerBucket").transform;
        Object toInstantiateSprite = Resources.Load("Prefabs/villager", typeof(GameObject));
        while (villagerCount < maxVillagers) {
            villagerCount++;
            Vector3 spritePosition = new Vector3(Random.Range(-10.0f, 10.0f), 1.0f, Random.Range(-10.0f, 10.0f));
            GameObject sprite = Instantiate(toInstantiateSprite, spritePosition, Quaternion.identity) as GameObject;
            sprite.GetComponent<Properties>().id = villagerCount;
            sprite.GetComponent<Properties>().SetJob(AssignmentCounter.counter.AssignJob());
            sprite.transform.SetParent(villagers);
        }
        spawned = true;
    }

    public void ReassignVillager(string from, string to) {
        foreach(Transform villager in villagers) {
            if (villager.gameObject.GetComponent<Properties>().baseJob == from) {
                villager.gameObject.GetComponent<Job>().ChangeJob(to);
                return;
            }
        }
    }
}
