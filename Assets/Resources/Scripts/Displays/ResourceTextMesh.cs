using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResourceTextMesh : MonoBehaviour {

    private bool updated;

    void Update() {
        if (updated) {
            return;
        }

        GameObject text = new GameObject();
        TextMeshPro t = text.AddComponent<TextMeshPro>();
        t.name = "label";
        t.text = transform.name;
        t.fontSize = 5;
        t.transform.localEulerAngles += new Vector3(0, 0, 90);
        t.transform.SetParent(transform);

        RectTransform rt = t.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(2, 0.5f);
        float yOffset = GetComponent<Renderer>().bounds.size.y + rt.sizeDelta.x / 5;
        rt.localPosition = new Vector3(0, yOffset, 0);

        updated = true;
    }
}
