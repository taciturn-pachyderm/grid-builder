using UnityEngine;

public class CollisionDetector : MonoBehaviour {

    public bool triggered = false;

    private void OnTriggerEnter(Collider other) {
        if (CheckTag(other)) {
            triggered = GetComponent<Properties>().type == TargetManager.manager.targetType;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (CheckTag(other)) {
            triggered = GetComponent<Properties>().type == TargetManager.manager.targetType;
        }
    }

    bool CheckTag(Collider other) {
        return other.gameObject.tag == "selection";
    }
}
