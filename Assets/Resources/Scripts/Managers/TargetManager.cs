﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour {

    public static TargetManager manager;

    public Vector3 hitDown;
    public Vector3 mousePos;
    float selectionAngle = 0f;
    Vector2 selectionDirection = new Vector2(0, 0);
    public GUISkin skin;
    private bool select;
    public string targetType;
    Dictionary<string, GameObject> selectedSprites = new Dictionary<string, GameObject>();
    public Dictionary<string, GameObject> harvestedSprites = new Dictionary<string, GameObject>();
    public bool placeable;
    public bool targetMode;
    public Dictionary<string, string> hotKeys = new Dictionary<string, string>();
    List<string> placeables = new List<string>() {"sawyer", "storage"};
    private Plane plane = new Plane(Vector3.up, Vector3.zero);
    private HashSet<string> selectionVectors = new HashSet<string>();
    private List<GameObject> selectionObjects = new List<GameObject>();
    Object selectionCubePrefab;
    bool drawGrid = false;
    public IEnumerator selectionBoxDraw;
    private bool selectionDrawBoxRunning;
    int startX;
    int startZ;
    int endX;
    int endZ;
    Material selectedRock;
    Material brokenRock;

    void Awake() {
        // singleton pattern
        if (manager == null) {
            manager = this;
        } else if (manager != this) {
            Destroy(gameObject);
        }
        SetHotKeys();
    }

    void Start() {
        selectionBoxDraw = DrawSelectionBox();
        LoadPrefabs();
    }

    void LoadPrefabs() {
        selectionCubePrefab = Resources.Load("Prefabs/selection", typeof(GameObject));

        selectedRock = Resources.Load<Material>("Materials/rock-selected");

        Object logPrefab = Resources.Load("Prefabs/logs", typeof(GameObject));
        GameObject theLogs = Instantiate(logPrefab, new Vector2(-10000, -10000), Quaternion.identity) as GameObject;
        theLogs.SetActive(false);
        harvestedSprites.Add("tree", theLogs);

        Object rubblePrefab = Resources.Load("Prefabs/rubble", typeof(GameObject));
        GameObject theRubble = Instantiate(rubblePrefab, new Vector2(-10000, -10000), Quaternion.identity) as GameObject;
        theRubble.SetActive(false);
        harvestedSprites.Add("rock", theRubble);
    }

    void SetHotKeys() {
        hotKeys.Add("1", "tree");
        hotKeys.Add("2", "rock");
        hotKeys.Add("9", "sawyer");
        hotKeys.Add("0", "storage");
        hotKeys.Add("escape", "none");
    }

	void Update() {
        // convert CheckInput to a Coroutine
        CheckInput();
        // this select var toggling was originally in place to prevent multiple coroutines from spawning
        // select = false;
        if (UIActive()) {
            return;
        }
        if (placeable) {
            BuildingManager.manager.PlaceBuilding(targetType);
            return;
        }
        if (targetType == "" || !CursorManager.manager.IsCurrentSelectedObjectSelectable()) {
            return;
        }
        // select = true;
        ManageGridDraw();
	}

    void CheckInput() {
        foreach (string key in hotKeys.Keys) {
            if (Input.GetKeyDown(key)) {
                string target = hotKeys[key];
                SetTarget(target);
                return;
            }
        }
    }

    void ManageGridDraw() {
        // can any of this boolean logic be cleaned up?
        if ((Input.GetMouseButtonUp(0) || Input.GetAxis("DrawGrid") == 0) && drawGrid && selectionDrawBoxRunning) {
            StopSelectionCoroutine();
            EndSelection();
        }
        if (!Input.GetMouseButton(0) && Input.GetAxis("DrawGrid") == 0) {
            // tracks mouse position/center of screen; stops updating when button is pressed
            hitDown = GetXZVectorFromScreen();
        }
        if ((Input.GetMouseButton(0) || Input.GetAxis("DrawGrid") != 0) && targetType != "" && !placeable && !selectionDrawBoxRunning) {
            // draws grid if mass-selectable target
            StartSelection();
        }
    }

    public void StopSelectionCoroutine() {
        drawGrid = false;
        StopCoroutine(selectionBoxDraw);
        selectionDrawBoxRunning = false;
    }

    private void StartSelection() {
        drawGrid = true;
        StartCoroutine(selectionBoxDraw);
        selectionDrawBoxRunning = true;
    }

    bool UIActive() {
        bool active = false;
        foreach (GameObject uiElement in GameObject.FindGameObjectsWithTag("ui-container")) {
            if (active) {
                break;
            }
            active = uiElement.GetComponent<UIState>().isActive;
        }
        return active;
    }

    void EndSelection() {
        AddSelectedObjectsToQueue();
        foreach (GameObject go in selectionObjects) {
            Destroy(go);
        }
        selectionVectors = new HashSet<string>();
        drawGrid = false;
    }

    void AddSelectedObjectsToQueue() {
        Vector3 center = new Vector3(startX + (endX - startX) / 2, 0.25f, startZ + (endZ - startZ) / 2);
        Vector3 size = new Vector3(Mathf.Abs(endX - startX) / 2, 10f, Mathf.Abs(endZ - startZ) / 2);
        Collider[] check = Physics.OverlapBox(center, size, Quaternion.identity, 1 << 8);
        foreach (Collider c in check) {
            if (targetType == c.gameObject.GetComponent<Properties>().type) {
                c.gameObject.GetComponent<Properties>().selected = true;
                c.gameObject.GetComponent<MeshRenderer>().material = selectedRock;
            }
        }
    }

    IEnumerator DrawSelectionBox() {
        while (true) {
            mousePos = GetXZVectorFromScreen();
            float theX = mousePos.x - hitDown.x;
            int hitX = (int)hitDown.x;
            int hitZ = (int)hitDown.z;
            int mouseX = (int)mousePos.x;
            int mouseZ = (int)mousePos.z;

            if (theX < 0) {
                startX = mouseX;
                endX = hitX;
            } else {
                startX = hitX;
                endX = mouseX;
            }

            float theZ = mousePos.z - hitDown.z;
            if (theZ < 0) {
                startZ = mouseZ;
                endZ = hitZ;
            } else {
                startZ = hitZ;
                endZ = mouseZ;
            }

            for (int i = startX; i < endX; i++) {
                for (int j = startZ; j < endZ; j++) {
                    if (selectionVectors.Add(i + "," + j)) {
                        MakeCube(i, j);
                    }
                }
            }

            foreach (GameObject go in selectionObjects) {
                if (go == null) {
                    continue;
                }
                Vector3 pos = go.transform.position;
                if (pos.x < startX || pos.x > endX || pos.z < startZ || pos.z > endZ) {
                    selectionVectors.Remove(pos.x + "," + pos.z);
                    Destroy(go);
                }
            }
            yield return null;
        }
    }

    void MakeCube(int x, int z) {
        Vector3 newVector = new Vector3(x, 0.125f, z);
        GameObject newObj = Instantiate(selectionCubePrefab, newVector, Quaternion.identity) as GameObject;
        selectionObjects.Add(newObj);
    }

    Vector3 GetXZVectorFromScreen() {
        Vector3 source = GamepadController.controller.enabled
            ? new Vector3(Screen.width / 2, Screen.height / 2, 0)
            : Input.mousePosition;
        Vector3 v3OrgMouse;
        Ray ray = Camera.main.ScreenPointToRay(source);
        float dist;
        plane.Raycast(ray, out dist);
        v3OrgMouse = ray.GetPoint(dist);
        v3OrgMouse.y = 0.5f;
        return new Vector3(Mathf.Round(v3OrgMouse.x), v3OrgMouse.y, Mathf.Round(v3OrgMouse.z));
    }

    public void SetTarget(string name) {
        placeable = placeables.Contains(name);
        targetType = name;
        CursorManager.manager.SetTarget(name);
    }
}
