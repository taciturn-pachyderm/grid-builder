using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorManager : MonoBehaviour {

    public static CursorManager manager;

    private Vector3 offset = new Vector3(20.0f, 0f, 0f);
    GameObject cursor;
    public bool selectable;
    public Dictionary<string, GameObject> cursorObjects = new Dictionary<string, GameObject>();
    public string currentSelection;
    public GameObject selectionPlane;

    void Awake() {
        // singleton pattern
        if (manager == null) {
            manager = this;
        } else if (manager != this) {
            Destroy(gameObject);
        }

        cursorObjects["storage"] = transform.Find("StorageCursor").gameObject;
        cursorObjects["storage"].SetActive(false);
        cursorObjects["sawyer"] = transform.Find("SawyerCursor").gameObject;
        cursorObjects["sawyer"].SetActive(false);
        cursorObjects["tree"] = transform.Find("TreeCursor").gameObject;
        cursorObjects["tree"].SetActive(false);
        cursorObjects["rock"] = transform.Find("RockCursor").gameObject;
        cursorObjects["rock"].SetActive(false);
        currentSelection = "none";
    }

    void Update() {
        transform.position = Input.mousePosition + offset;
    }

    // this is the worst
    public void SetTarget(string name) {
        if (name == "none" && currentSelection == "none") {
            TargetManager.manager.targetType = null;
            return;
        }
        if (currentSelection != "none") {
            cursorObjects[currentSelection].SetActive(false);
        }
        SetCursorImage(name);
    }

    public void SetCursorImage(string name) {
        currentSelection = name;
        if (name != "none") {
            cursorObjects[currentSelection].SetActive(name != "none");
        }
    }

    public bool IsCurrentSelectedObjectSelectable() {
        if (currentSelection == "none") {
            return false;
        }
        return cursorObjects[currentSelection].GetComponent<SelectorID>().selectable;
    }

    public Vector3 GetPlacementLocation() {
        return cursorObjects[currentSelection].transform.position;
    }
}
