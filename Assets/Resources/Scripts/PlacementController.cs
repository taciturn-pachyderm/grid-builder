﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementController : MonoBehaviour {

    private Vector3 v3OrgMouse;
    private Plane plane = new Plane(Vector3.up, Vector3.zero);

	void Update() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float dist;
        plane.Raycast(ray, out dist);
        v3OrgMouse = ray.GetPoint(dist);
        v3OrgMouse.y = 0.5f;
        this.transform.position = new Vector3(Mathf.Round(v3OrgMouse.x), v3OrgMouse.y, Mathf.Round(v3OrgMouse.z));
    }
}
